<?php
namespace Model;

class Patient extends DBConnect
{
    protected $table;

    public function __construct($table)
    {
        parent::__construct();
        $this->table = $table;
    }

    public function getDataById($id)
    {
        $result = $this->query("SELECT a.*,
                               b.description as diagnos_description,
                               b.date_created as date_diagnos
                               from {$this->table} as a
                               LEFT JOIN diagnosis as b ON a.pid = b.pid
                               WHERE a.pid = {$id}");
        $data = $this->fetchAssoc($result);
        $this->free($result);
        $this->close();
        return $data == null ? array() : $data;
    }

    public function getAllData()
    {
        $data = array();
        $result = $this->query("SELECT a.*,
                               b.description as diagnos_description,
                               b.date_created as date_diagnos
                               from {$this->table} as a
                               LEFT JOIN diagnosis as b ON a.pid = b.pid");
        if($result){
            while($row = $this->fetchAssoc($result)){
                $data[] = $row;
            }
        }
        $this->close();
        return $data;
    }

    public function addData($data)
    {
        $datetime = date('Y-m-d H:i:s');
        $res = '';
        $result = $this->query("INSERT INTO {$this->table} VALUES(
                               null,
                               '{$data['fname']}',
                               '{$data['lname']}',
                               '{$data['address']}',
                               '{$data['phone']}',
                               '{$data['age']}',
                               '{$data['height']}',
                               '{$data['weight']}',
                               '{$data['sex']}',
                               '{$datetime}',
                               '{$datetime}')");
        if ($result)
            $res = "The data was successfully added.";
        else
            $res = "There was an error in adding the data.";
        $this->close();
        return $res;
    }

    public function removeData($id)
    {
        $result = $this->query("DELETE from {$this->table} where pid = {$id}");
        $this->close();
        if ($result)
            return "Successfully removed.";
        else
            return "There was an error in deleting the data.";
    }

    public function updateData($data)
    {
        $datetime = date('Y-m-d H:i:s');
        $str = array();
        if (isset($data['fname']))
            $str[] = "fname = '{$data['fname']}'";
        if (isset($data['lname']))
            $str[] = "lname = '{$data['lname']}'";
        if (isset($data['address']))
            $str[] = "address = '{$data['address']}'";
        if (isset($data['phone']))
            $str[] = "phone = '{$data['phone']}'";
        if (isset($data['age']))
            $str[] = "age = '{$data['age']}'";
        if (isset($data['height']))
            $str[] = "height = '{$data['height']}'";
        if (isset($data['weight']))
            $str[] = "weight = '{$data['weight']}'";
         if (isset($data['sex']))
            $str[] = "sex = '{$data['sex']}'";
    
        if (empty($str))
            return "Empty parameters found.";
        $str[] = "date_updated = '{$datetime}'";
        $str = implode(',', $str);
    
        $result = $this->query("UPDATE {$this->table} SET {$str} where pid = {$data['id']}");
        $this->close();
        if ($result)
            return "Successfully updated.";
        else
            return "There was an error in updating the data.";
    }
}

?>
