<?php
namespace Model;

class PhysicalExam extends DBConnect
{
    protected $table;

    public function __construct($table)
    {
        parent::__construct();
        $this->table = $table;
    }

    public function getDataById($id)
    {
        $result = $this->query("SELECT *
                               from {$this->table}
                               WHERE peid = {$id}");
        $data = $this->fetchAssoc($result);
        $this->free($result);
        $this->close();
        return $data == null ? array() : $data;
    }

    public function getAllData()
    {
        $data = array();
        $result = $this->query("SELECT *
                               from {$this->table}");
        if($result){
            while($row = $this->fetchAssoc($result)){
                $data[] = $row;
            }
        }
        $this->close();
        return $data;
    }

    public function addData($data)
    {
        $datetime = date('Y-m-d H:i:s');
        $pid = intval($data['pid']);
        $res = '';
        $result = $this->query("INSERT INTO {$this->table} VALUES(
                               null,
                               {$pid},
                               '{$data['bp']}',
                               '{$data['hr']}',
                               '{$data['temperature']}',
                               '{$datetime}',
                               '{$datetime}')");
        if ($result)
            $res = "The data was successfully added.";
        else
            $res = "There was an error in adding the data.";
        $this->close();
        return $res;
    }

    public function removeData($id)
    {
        $result = $this->query("DELETE from {$this->table} where peid = {$id}");
        $this->close();
        if ($result)
            return "Successfully removed.";
        else
            return "There was an error in deleting the data.";
    }

    public function updateData($data)
    {
        $pid = intval($data['pid']);
        $datetime = date('Y-m-d H:i:s');
        $str = array();
        if (isset($data['pid']))
            $str[] = "pid = '{$pid}'";
        if (isset($data['bp']))
            $str[] = "bp = '{$data['bp']}'";
        if (isset($data['hr']))
            $str[] = "hr = '{$data['hr']}'";
        if (isset($data['temperature']))
            $str[] = "temperature = '{$data['temperature']}'";
    
        if (empty($str))
            return "Empty parameters found.";
        $str[] = "date_updated = '{$datetime}'";
        $str = implode(',', $str);
        
        $result = $this->query("UPDATE {$this->table} SET {$str} where peid = {$data['id']}");
        $this->close();
        if ($result)
            return "Successfully updated.";
        else
            return "There was an error in updating the data.";
    }
}

?>
