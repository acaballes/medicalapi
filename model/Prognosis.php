<?php
namespace Model;

class Prognosis extends DBConnect
{
    protected $table;

    public function __construct($table)
    {
        parent::__construct();
        $this->table = $table;
    }

    public function getDataById($id)
    {
        $result = $this->query("SELECT *
                               from {$this->table}
                               WHERE prog_id = {$id}");
        $data = $this->fetchAssoc($result);
        $this->free($result);
        $this->close();
        return $data == null ? array() : $data;
    }

    public function getAllData()
    {
        $data = array();
        $result = $this->query("SELECT *
                               from {$this->table}");
        if($result){
            while($row = $this->fetchAssoc($result)){
                $data[] = $row;
            }
        }
        $this->close();
        return $data;
    }

    public function addData($data)
    {
        $datetime = date('Y-m-d H:i:s');
        $did = intval($data['did']);
        $res = '';
        $result = $this->query("INSERT INTO {$this->table} VALUES(
                               null,
                               '{$data['description']}',
                               {$did},
                               '{$datetime}',
                               '{$datetime}')");
        if ($result)
            $res = "The data was successfully added.";
        else
            $res = "There was an error in adding the data.";
        $this->close();
        return $res;
    }

    public function removeData($id)
    {
        $result = $this->query("DELETE from {$this->table} where prog_id = {$id}");
        $this->close();
        if ($result)
            return "Successfully removed.";
        else
            return "There was an error in deleting the data.";
    }

    public function updateData($data)
    {
        $did = intval($data['did']);
        $datetime = date('Y-m-d H:i:s');
        $str = array();
        if (isset($data['description']))
            $str[] = "description = '{$data['description']}'";
        if (isset($data['did']))
            $str[] = "did = '{$did}'";
    
        if (empty($str))
            return "Empty parameters found.";
        $str[] = "date_updated = '{$datetime}'";
        $str = implode(',', $str);
        
        $result = $this->query("UPDATE {$this->table} SET {$str} where prog_id = {$data['id']}");
        $this->close();
        if ($result)
            return "Successfully updated.";
        else
            return "There was an error in updating the data.";
    }
}

?>
