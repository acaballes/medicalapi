<?php
/*
 * @author Algie Caballes
 * @desc abstract class to connect to database
 */
namespace Model;

abstract class DBConnect
{
    protected $mysqli;
    
    public function __construct()
    {
        $this->mysqli = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
        if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
        }	
    }
    
    protected function close()
    {
        mysqli_close($this->mysqli);
    }
    
    protected function query($str)
    {
         $result = mysqli_query($this->mysqli, $str);
         return $result;
    }
    
    protected function fetchAssoc($result)
    {
        return mysqli_fetch_assoc($result);
    }
    
    protected function free($result)
    {
        mysqli_free_result($result);
    }
}