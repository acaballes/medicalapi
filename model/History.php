<?php
namespace Model;

class History extends DBConnect
{
    protected $table;

    public function __construct($table)
    {
        parent::__construct();
        $this->table = $table;
    }

    public function getDataById($id)
    {
        $result = $this->query("SELECT *
                               from {$this->table}
                               WHERE hid = {$id}");
        $data = $this->fetchAssoc($result);
        $this->free($result);
        $this->close();
        return $data == null ? array() : $data;
    }

    public function getAllData()
    {
        $data = array();
        $result = $this->query("SELECT *
                               from {$this->table}");
        if($result){
            while($row = $this->fetchAssoc($result)){
                $data[] = $row;
            }
        }
        $this->close();
        return $data;
    }

    public function addData($data)
    {
        $datetime = date('Y-m-d H:i:s');
        $res = '';
        $result = $this->query("INSERT INTO {$this->table} VALUES(
                               null,
                               '{$data['pid']}',
                               '{$data['did']}',
                               '{$data['meid']}',
                               '{$data['peid']}',
                               '{$data['prog_id']}',
                               '{$data['rid']}',
                               '{$datetime}',
                               '{$datetime}')");
        if ($result)
            $res = "The data was successfully added.";
        else
            $res = "There was an error in adding the data.";
        $this->close();
        return $res;
    }

    public function removeData($id)
    {
        $result = $this->query("DELETE from {$this->table} where hid = {$id}");
        $this->close();
        if ($result)
            return "Successfully removed.";
        else
            return "There was an error in deleting the data.";
    }

    public function updateData($data)
    {
        $datetime = date('Y-m-d H:i:s');
        $str = array();
        if (isset($data['pid']))
            $str[] = "pid = '{$data['pid']}'";
        if (isset($data['did']))
            $str[] = "did = '{$data['did']}'";
        if (isset($data['meid']))
            $str[] = "meid = '{$data['meid']}'";
        if (isset($data['peid']))
            $str[] = "peid = '{$data['peid']}'";
        if (isset($data['prog_id']))
            $str[] = "prog_id = '{$data['prog_id']}'";
        if (isset($data['rid']))
            $str[] = "rid = '{$data['rid']}'";
    
        if (empty($str))
            return "Empty parameters found.";
        $str[] = "date_updated = '{$datetime}'";
        $str = implode(',', $str);
        
        $result = $this->query("UPDATE {$this->table} SET {$str} where hid = {$data['id']}");
        $this->close();
        if ($result)
            return "Successfully updated.";
        else
            return "There was an error in updating the data.";
    }
}

?>
