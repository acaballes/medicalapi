<?php
/**
 * @author Algie Caballes <algie.developer@gmail.com>
 * @desc index that handles requests
 */
ini_set('display_errors', 1);
require_once('config/config.php');
require_once('AbstractResource.php');
require_once('ApiResource.php');
require_once('model/DBConnect.php');
require_once('model/Patient.php');
require_once('model/Diagnosis.php');
require_once('model/Prognosis.php');
require_once('model/PhysicalExam.php');
require_once('model/MedicalExam.php');
require_once('model/History.php');
require_once('model/Remarks.php');

use Api\ApiResource;

try {
    $resource = new ApiResource($_SERVER, $_REQUEST);
    echo $resource->processRequest();
} catch (Exception $e) {
    echo json_encode(Array('error' => $e->getMessage()));
}

?>
