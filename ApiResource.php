<?php
/**
 * @author Algie Caballes <algie.developer@gmail.com>
 * @desc Api resource page that handle api request
 */
namespace Api;

use Api\AbstractResource;
use Model\Query;

class ApiResource extends AbstractResource
{
    protected $authenticate = true;
	protected $query;
	
    public function __construct($server, $request)
    {
		parent::__construct($server, $request);
		// authorize header token access
		//if (!isset($server['HTTP_AUTHENTICATON_HEADER']) || $server['HTTP_AUTHENTICATON_HEADER'] != AUTH_TOKEN)
		//	$this->authenticate = false;
    }

	// patient API REST service
    protected function patient()
    {
		return $this->resultData(new \Model\Patient($this->api_name));
		
    }
	
	// diagnosis API REST service
	protected function diagnosis()
	{
		return $this->resultData(new \Model\Diagnosis($this->api_name));
	}
	
	// prognosis API REST service
	protected function prognosis()
	{
		return $this->resultData(new \Model\Prognosis($this->api_name));
	}
	
	// physical_exam API REST service
	protected function physical_exam()
	{
		return $this->resultData(new \Model\PhysicalExam($this->api_name));
	}
	
	// medical_exam API REST service
	protected function medical_exam()
	{
		return $this->resultData(new \Model\MedicalExam($this->api_name));
	}
	
	// remarks API REST service
	protected function remarks()
	{
		return $this->resultData(new \Model\Remarks($this->api_name));
	}
	
	// history API REST service
	protected function history()
	{
		return $this->resultData(new \Model\History($this->api_name));
	}
	
	// authenticate API REST service
	protected function authenticate()
	{
		return $this->resultData(new \Model\Authenticate($this->api_name));
	}
 }
?>
