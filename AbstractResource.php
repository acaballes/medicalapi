<?php
/**
 * @author Algie Caballes <algie.developer@gmail.com>
 * @desc An abstract class that handle api request
 */
namespace Api;

abstract class AbstractResource
{
    protected $method = '';
    protected $request_data = array();
    protected $request = '';
    protected $api_name = '';

    public function __construct($server, $request) {
        header("Accept: application/json");
        header("Content-Type: application/json");
	$this->request = $request;
	$this->method = $server['REQUEST_METHOD'];
	$this->getRequestData();
	$this->api_name = isset($this->request['api']) ? $this->request['api'] : null;
    }

    protected function getRequestData()
    {
	switch($this->method) {
        case 'DELETE':
	    $this->request_data = $_GET;
            break;
        case 'POST':
            $this->request_data = $_POST;
            break;
        case 'GET':
            $this->request_data = $_GET;
            break;
        case 'PUT':
            $this->request_data = $_GET;
            break;
        default:
            $this->response('Invalid Method', 405);
            break;
        }
    }

    public function processRequest()
    {
	if (method_exists($this, $this->api_name)) {
	    $res = $this->{$this->api_name}($this->request_data);
            return $this->response($res[0], $res[1]);
        } else {
	    return $this->response("Rest api {$this->api_name} not found", 404);
	}
    }

    private function response($data, $status)
    {
        header("HTTP/1.1 " . $status . " " . $this->status($status));
		return json_encode(is_array($data) ? array_merge(array('status' => $this->status($status)),array('result' => $data)) : array('status' => $this->status($status), 'result' => $data));
    }

    private function status($status)
    {
	$status_codes = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status_codes[$status])?$status_codes[$status]:$status_codes[500];
    }
	
	protected function resultData($query)
	{
		if (!$this->authenticate)
			return array("Error authenticating your access token.", 405);
		// GET REQUEST
        if ($this->method == 'GET') {
			// REQUEST PATIENT BY ITS ID
			if (isset($this->request_data['id'])) {
				return array($query->getDataById($this->request_data['id']), 200);
			} else {
				//REQUEST ALL PATIENT
				return array($query->getAllData(), 200);
	    }

		// POST REQUEST
        } else if ($this->method == 'POST') {
			if (!empty($this->request_data))
				return array($query->addData($this->request_data), 200);	
			else
				return array("Post data is empty.", 500);
	
		// DELETE REQUEST
        } else if ($this->method == 'DELETE') {
			if (isset($this->request_data['id'])) {
				return array($query->removeData($this->request_data['id']),200);
			} else {
				return array("Id is not defined.", 500);
			}
		
		//PUT  REQUEST
        } else if ($this->method == 'PUT') {
            if (isset($this->request_data['id'])) {
                return array($query->updateData($this->request_data), 200);
            } else {
                return array("Id is not defined for PUT request.", 500);
            }
        }
	}

}

?>
